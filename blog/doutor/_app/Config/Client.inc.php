<?php
if (strpos($host, 'localhost') !== false) {
    // LOCALHOST
    define('HOST', 'localhost');
    define('USER', 'root');
    define('PASS', '');
    define('DBSA', 'admin_teste');
} elseif (strpos($host, 'mpitemporario.com.br/projetos') !== false) {
    // CONTENÇÃO
    define('HOST', 'localhost');
    define('USER', 'mpi_name');
    define('PASS', 'pass');
    define('DBSA', 'mpi_name');
} elseif (strpos($host, 'producao.mpitemporario') !== false) {
    // PRODUCAO
    define('HOST', 'localhost');
    define('USER', 'producao_blog1');
    define('PASS', 'pass');
    define('DBSA', 'producao_blog1');
} else {
    // PUBLICADO
    define('HOST', '192.168.3.130');
    define('USER', 'admin_oficin');
    define('PASS', 'NUfFqsWrop');
    define('DBSA', 'admin_oficin');
}
//
// DEFINE IDENTIDADE DO SITE
define('SITE_NAME', 'Oficina Laser'); // Nome do site do cliente
define('SITE_SUBNAME', 'Oficina Laser'); // Slogan
define('SITE_DESC', 'Oficina Laser - Simplesmente o melhor do ramo!...'); // Descrição do site do cliente até 160 caractéres
define('SITE_FONT_NAME', 'Lato'); // Tipografia do site (https:// www.google.com/fonts)
define('SITE_FONT_WHIGHT', '400,300,700,900'); // Tipografia do site (https:// www.google.com/fonts)
//
// DADOS DO SEU CLIENTE/DONO DO SITE
define('SITE_ADDR_NAME', 'Grupo Ideal Trends'); // Nome de remetente
define('SITE_ADDR_RS', 'Grupo Ideal Trends LTDA'); // Razão Social
define('SITE_ADDR_EMAIL', 'contato@idealtrends.com.br'); // E-mail de contato
define('SITE_ADDR_SITE', 'http:// www.idealtrends.com.br'); // URL descrita
define('SITE_ADDR_CNPJ', '00.000.000/0000-00'); // CNPJ da empresa
define('SITE_ADDR_IE', '000/0000000'); // Inscrição estadual da empresa
define('SITE_ADDR_PHONE_A', '(11) 9999-9999'); // Telefone 1
define('SITE_ADDR_PHONE_B', '(11) 0.0000-0000'); // Telefone 2
define('SITE_ADDR_ADDR', 'Rua Pequetita, 301'); // ENDEREÇO: rua, número (complemento)
define('SITE_ADDR_CITY', 'São Paulo'); // ENDEREÇO: cidade
define('SITE_ADDR_DISTRICT', 'Vila Olímpia'); // ENDEREÇO: bairro
define('SITE_ADDR_UF', 'SP'); // ENDEREÇO: UF do estado
define('SITE_ADDR_ZIP', '00000-000'); // ENDEREÇO: CEP
define('SITE_ADDR_COUNTRY', 'Brasil'); // ENDEREÇO: País
define('SITE_ADDR_COUNTRY_SIGLA', 'BR'); // ENDEREÇO: País
define('SITE_CORDENADAS', '');
// 
// Define o perfil do template
// Habilite e configure os widget ou APPs do template
define("APP", true); // Ative ou desativa os APPs/Widgets do template
// 
// Widgets
// Ativação de carrinhos de orçamento, banners, newsletter etc...
if (APP):
define('WIDGET_SEARCH', false); // ADICIONA A BARRA DE PESQUISA
define('WIDGET_CART', false); // ADICIONA OS BOTÕES E MODELOS DOS PRODUTOS PARA ADICIONAR AO CARRINHO
define("WIDGET_LANG", false); // false para desativar a lista de idiomas
define("WD_BANNER", true); // false para desativar e (int) para modelo do banner a ser aplicado
define("PROD_BREVEDESC", false); // false para desativar a breve descrição do produto
define("BLOG_BREVEDESC", true); // false para desativar a breve descrição do produto
define("CAT_CONTENT", true); // false para desativar a descrição da categoria
define("MIN_PER_PAGE", 6); // Quantidade mínima por página
define("MULTIPLO_PAGE", 6); // Número de Selects das páginas multiplicado
// Escolha um modelo de galeria e sete abaixo, somente uma pode estar true
define("GALLERY_DEFAULT", true); 
define("GALLERY_CUSTOM", false); 
endif;define('TOKEN_API', 'b7185c6191c839d9');
