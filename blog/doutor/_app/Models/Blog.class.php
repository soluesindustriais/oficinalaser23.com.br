<?php

/**
 * Blog.class.php [MODEL]
 * Classe responsável por gerir as postagens do blog no banco de dados.
 * @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
 */
class Blog {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  //Entrada de dados
  private $Data;
  private $Id = null;
  private $File;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * @param array $Data
   */
  public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();
    $this->CheckData();

    if ($this->Result):
      $this->Create();
    endif;
  }

  public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();

    $this->Id = $this->Data['blog_id'];
    unset($this->Data['blog_id']);

    $this->CheckData();

    if ($this->Result):
      $this->Update();
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica se algum campo que não é obrigatorio está vazio e remove do array

  private function CheckUnset() {
    if (empty($this->Data['blog_status'])):
      unset($this->Data['blog_status']);
    endif;
    if (empty($this->Data['blog_cover'])):
      unset($this->Data['blog_cover']);
    endif;
    if (empty($this->Data['blog_keywords'])):
      $this->Data['blog_keywords'] = ' ';
    endif;
    if (empty($this->Data['blog_brevedescription'])):
      $this->Data['blog_brevedescription'] = ' ';
    endif;
    if (!empty($this->Data['gallery_file'])):
      $this->File = $this->Data['gallery_file'];
      unset($this->Data['gallery_file']);
    endif;
  }

  //Verifica a integridade dos dados e direciona as operações
  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif (Check::CategoryRepeat($this->Data['blog_title'], $_SESSION['userlogin']['user_empresa'])):
      $this->Error = array("Já existe uma categoria ou sessão com o titulo: <b>{$this->Data['blog_title']}</b>, tente utilizar outro titulo.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif ($this->CheckNome()):
      $this->Error = array("Já existe uma postagem com este mesmo nome, tente outro", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif ($this->CheckGallery()):
      $this->Error = array("Você pode ter no máximo 15 imagens por galeria", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->sendFoto();
      $this->SetNull();
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

  //Verifica se existem itens repetidos
  private function CheckNome() {
    $this->Data['blog_name'] = Check::Name($this->Data['blog_title']);
    $WHERE = (!empty($this->Id) ? "blog_id != {$this->Id} AND" : null);
    $Read = new Read;
    $Read->ExeRead(TB_BLOG, "WHERE {$WHERE} blog_name = :cat", "cat={$this->Data['blog_name']}");
    if ($Read->getResult()):
      return true;
    else:
      return false;
    endif;
  }

  //Verifica a quantidade de fotos na galeria
  private function CheckGallery() {
    if (!in_array('', $this->File['tmp_name'])):
      $ReadGd = new Read;
      $ReadGd->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id", "id={$this->Id}");
      if ($ReadGd->getResult()):
        $soma = $ReadGd->getRowCount() + count($this->File['tmp_name']);
        if ($soma > 15):
          return true;
        endif;
      endif;
    else:
      return false;
    endif;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_BLOG, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Id = $Create->getResult();
      $this->Result = true;
      $this->SendGallery();
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Cadastrou a postagem {$this->Data['blog_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      Check::UpdateSitemap("../../sitemap.xml");
    endif;
  }

  //Envio da galeria
  private function SendGallery() {

    $gbFiles = array();
    $gbCount = count($this->File['tmp_name']);
    $gbKeys = array_keys($this->File);

    for ($gb = 0; $gb < $gbCount; $gb++):
      foreach ($gbKeys as $Keys):
        $gbFiles[$gb][$Keys] = $this->File[$Keys][$gb];
      endforeach;
    endfor;

    $Upload = new Upload;
    $insertGb = new Create;
    $i = 0;
    $u = 0;

    foreach ($gbFiles as $gbUpload):
      $i++;
      $ImgName = "blog-{$this->Data['blog_name']}-" . (substr(md5(time() + $i), 0, 10));
      $Upload->Image($gbUpload, $ImgName, 800, $_SESSION['userlogin']['user_empresa'], 'blog');

      if ($Upload->getResult()):
        $gbImage = $Upload->getResult();
        $gbCreate = array('user_empresa' => $_SESSION['userlogin']['user_empresa'], 'cat_parent' => $this->Data['cat_parent'], 'gallery_rel' => $this->Id, "gallery_file" => $gbImage);
        $insertGb->ExeCreate(TB_GALLERY, $gbCreate);
        $u++;
      endif;
    endforeach;
  }

  //Verifica e envia a foto do perfil do usuário para pasta da (int) empresa/user
  private function sendFoto() {
    if (!empty($this->Data['blog_cover']['tmp_name'])):
      $this->checkCover();
      $Upload = new Upload;
      $ImgName = "blog-{$this->Data['blog_name']}-" . (substr(md5(time() + $this->Data['blog_title']), 0, 10));
      $Upload->Image($this->Data['blog_cover'], $ImgName, 800, $_SESSION['userlogin']['user_empresa'], 'blog');
      if ($Upload->getError()):
        $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
        $this->Result = false;
      else:
        $this->Data['blog_cover'] = $Upload->getResult();
        $this->Result = true;
      endif;
    else:
      $this->Result = true;
    endif;
  }

  //Verifica se já existe uma foto, se sim deleta para enviar outra!
  private function checkCover() {
    if (!empty($this->Id)):
      $readCapa = new Read;
      $readCapa->ExeRead(TB_BLOG, "WHERE blog_id = :id", "id={$this->Id}");
      $capa = $readCapa->getResult();
      if ($readCapa->getResult()):
        $delCapa = $capa[0]['blog_cover'];
        if (file_exists("uploads/{$delCapa}") && !is_dir("uploads/{$delCapa}")):
          unlink("uploads/{$delCapa}");
        endif;
      endif;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_BLOG, $this->Data, "WHERE blog_id = :id", "id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->SendGallery();
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Blog", "Atualizou a postagem {$this->Data['blog_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
      Check::UpdateSitemap("../../sitemap.xml");
    endif;
  }

}
