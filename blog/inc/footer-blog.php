<div class="clear"></div>
<footer>
    <div class="wrapper">
        <div class="row">
            <div class="p-3 col-12 col-md-12 col-lg-12 my-2">
                <div class="d-flex justify-content-center">
                    <? include('inc/canais.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__menu">
        <nav>
            <ul>
                <?php
                foreach ($menu as $key => $value) {
                    if ($sigMenuPosition !== false && $key == $sigMenuPosition) include('inc/menu-footer-sig.php');
                    echo '
                    <li>
                    <a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : ($value[0] === '' ? $url . $value[0] : $urlBase . $value[0])) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false || strpos($value[0], '.pdf') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
                    </li>
                    ';
                } ?>
                <li><a href="<?= $urlBase ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
            </ul>
        </nav>
    </div>
</footer>
<div class="copyright-footer">
    <div class="wrapper">
        Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
        <div class="selos">
            <a rel="nofollow" href="https://validator.w3.org/check?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5" aria-hidden="true"></i> <strong>W3C</strong></a>
            <a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&amp;profile=css3svg&amp;usermedium=all&amp;warning=1&amp;vextwarning=&amp;lang=pt-BR" target="_blank" title="CSS W3C"><i class="fab fa-css3-alt" aria-hidden="true"></i> <strong>W3C</strong></a>
            <img class="object-fit-contain" src="<?= $url ?>imagens/selo.png" alt="Desenvolvido com MPI Technology®" title="Desenvolvido com MPI Technology®" width="50" height="41" loading="lazy">
        </div>
    </div>
    <div class="clear"></div>
</div>


<? include('inc/LAB.php'); ?>

<!-- end footer -->

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-M0X26DWZKG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-M0X26DWZKG');
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>

<?php if (WIDGET_LANG) : ?>
    <script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
    <script>
        <? include('js/gtranslate.js'); ?>
    </script>
<?php endif; ?>

