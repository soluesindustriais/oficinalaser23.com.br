<?
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];

if ($dir["dirname"] == "/") {
    $url = $http."://".$host."/";
}else {
    $url = $http."://".$host.$dir["dirname"]."/";
}

if (strpos($url, 'blog/')) {
    $urlBase = str_replace(['blog/'], '', $url);
    $GLOBALS['DIR'] = '../';
} else {
    $urlBase = $url;
}

$isLocalhost = strpos($host, 'localhost') !== false ? true : false;

$nomeSite           = 'Oficina Laser';
$slogan             = 'Oficina Laser - Simplesmente o melhor do ramo!...';

//AUTO DESCRIPTION
$auto_desc = $h1 . ' - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam';

// $url             = 'http://localhost/site_base_full_beta/';23.6083756
// $url             = 'http://mpitemporario.com.br/projetos/site_base_full_beta/';

$ddd                = '11';
$fone[0]            = array($ddd,'1111-1111','fas fa-phone');
$fone[1]            = array($ddd,'92222-2222','fab fa-whatsapp');

$whatsapp           = $ddd.'92222-2222';

$emailContato       = 'contato@doutoresdaweb.com.br';

$rua                = 'Av. Engenheiro Luís Carlos Berrini, 1297';
$bairro             = 'Itaim Bibi';
$cidade             = 'São Paulo';
$UF                 = 'SP';
$cep                = 'CEP: 04571-010';

$mapa               = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.887269188617!2d-46.69574088497707!3d-23.608375569215365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cb1288720d%3A0x6806a285858e9a97!2sAv.%20Engenheiro%20Lu%C3%ADs%20Carlos%20Berrini%2C%201297%20-%20Itaim%20Bibi%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004571-010!5e0!3m2!1spt-BR!2sbr!4v1661112493683!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';
// MAP CLEANUP
preg_match('/(?<=src=").*?(?=[\"])/', $mapa, $out);
$mapa = $out[0];

$idCliente          = 'ID_do_cliente_no_mpi_sistema';
$idAnalytics        = 'ID_do_Google_Analytics';
$senhaEmailEnvia    = '102030'; // COLOCAR SENHA DO E-MAIL MKT@DOMINIODOCLIENTE.COM.BR 

$explode            = explode("/", $_SERVER['REQUEST_URI']);
$urlPagina          = end($explode);
$urlPagina          = str_replace('.php','',$urlPagina);
$urlPagina          == "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);

// RECAPTCHA DO GOOGLE
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';

// ******************** MENU ********************
$sigMenuPosition = 1; // EX: $sigmenuposition = 3;
$sigMenuIcons = []; // EX: $sigMenuIcons = ['cogs', 'user', ...]

$menu[0] = array('','Home', false, NULL);
$menu[1] = array('sobre-nos','Sobre Nós', false, NULL);
// $menu[2] = array('produtos','Produtos', 'sub-menu-blog', NULL);

// ******************** PASTA DE IMAGENS, GALERIA, URL FACEBOOK, ETC. ********************
$pasta  = 'imagens/informacoes/';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco

// ******************** REDES SOCIAIS ********************
// $paginaFacebook      = 'doutoresdaweboficial';
// $urlInstagram        = 'URL_COMPLETA_INTAGRAM';
// $urlYouTube          = 'URL_COMPLETA_YOUTUBE';
// $urlLinkedIn         = 'URL_COMPLETA_LINKEDIN';
// $urlTwitter          = 'URL_COMPLETA_TWITTER';
// $urlTikTok           = 'URL_COMPLETA_TIKTOK';
// $urlThreads          = 'URL_COMPLETA_THREADS';

/* BREADCRUMBS MANUAIS */
$caminho = '
<div class="bread bread--default">
    <div class="wrapper">
        <div class="bread__row">
            <nav aria-label="breadcrumb">
                <ul id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="bread__column" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a href="' . $url . '" itemprop="item" title="Home">
                            <span itemprop="name">Home</span>
                        </a>
                        <meta itemprop="position" content="1">
                    </li>
                    <li class="bread__column active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <span itemprop="name">'.$h1.'</span>
                        <meta itemprop="position" content="2">
                    </li>
                </ul>
            </nav>
            <h1 class="bread__title">'.$h1.'</h1>
        </div>
    </div>
</div>
';


// ******************** PAGE LOADING ANIMATION ********************
$pageLoadingAnimation = false; //Habilita animações de carregamento
$pageLoadingAllPages = true; //Animação em todas as páginas ou apenas na index/home (false = index, true = todas) 
$pageLoadingTimeout = 200; //Adiciona tempo extra na animação, a contagem é iniciada após o carregamento da página. Valor em milisegundos (1 segundo = 1000 milisegundos).

$pageLoadingLogo = true; //Animação com logo
$pageLoadingSpinner = true; //Animação com spinner
//OBS: É possível usar as duas animações simultaneamente

// ******************** VARIÁVEIS DE TESTE PARA ENVIO DE E-MAIL ********************
// 1 - Altere para ** TRUE ** para realizar o teste de envio dos formulários do site e volte para ** FALSE ** após finalizá-lo
$envioTeste = true;
// 2 - Insira seu email para fazer o teste de envio dos formulários do site
$emailTeste = 'teste@doutoresdaweb.com.br';

// ******************** MOBILE DETECT ********************
$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);

// ******************** WHATSAPP LINK  ********************

if(!function_exists('wppLink')) {
    function wppLink($phoneNumber, $altMessage = null) {
        global $isMobile, $nomeSite;
        $wppNumber = is_array($phoneNumber) ? $phoneNumber[0].$phoneNumber[1] : $phoneNumber;
        $wppVerifyDevice    = $isMobile ? 'api' : 'web';
        $wppClearStr        = array(' ', '-', '(', ')');
        $defaultMessage = "Olá! Gostaria de mais informações sobre as ofertas da {$nomeSite}";
        $wppMessage = isset($altMessage) && !empty($altMessage) ? $altMessage : $defaultMessage;
        return 'https://'.$wppVerifyDevice.'.whatsapp.com/send?phone=55'.str_replace($wppClearStr, '', $wppNumber).'&amp;text='.rawurlencode($wppMessage);
    }
}

// ******************** STR UTILITIES *****************
include('inc/str-utilities.php');

// ******************** ARR PREPOSIÇÕES *****************
$prepos = array(' a ',' á ',' à ',' ante ',' até ',' após ',' de ',' desde ',' em ',' entre ',' com ',' para ',' por ',' perante ',' sem ',' sob ',' sobre ',' na ',' no ',' e ',' do ',' da ',' ','(',')','\'','"','.','/',':',' | ', ',, ');

// ******************** GET COORDINATES ********************
if(!function_exists('GetCoordinates')) {
    function GetCoordinates($map){
        preg_match_all('/!2d([0-9|.|-]+)\!3d([0-9|.|-]+)/', $map, $coordinates, PREG_SET_ORDER, 0);
        return $coordinates[0][2].';'.$coordinates[0][1];
    }
}

// ******************** GOOGLE FONTS TAG AND CALL CSS GENERATOR ********************
include('inc/gf-fonts.php');
?>
