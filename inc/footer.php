<footer>
    <div class="wrapper">
        <div class="contact-footer">
            <address>
                <span><?= $nomeSite . " - " . $slogan ?></span>
            </address>
        </div>
        <div class="menu-footer">
            <nav>
                <ul>
                    <li><a rel="nofollow" href="<?= $url ?>" title="Página inicial">Início</a></li>
                    <li><a rel="nofollow" href="<?= $url ?>sobre-nos" title="Sobre Nós">Sobre Nós</a></li>
                    <li><a rel="nofollow" href="<?= $url ?>produto" title="Produtos">Produtos</a></li>

                    <li><a href="<?= $url ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
                </ul>
            </nav>
        </div>
        <br class="clear">
    </div>
</footer>
<div class="copyright-footer">
    <div class="wrapper-footer">
        Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
        <div class="center-footer">
            <img style="width: 30%;" src="imagens/img-home/logo-footer.png" alt="<?= $nomeSite ?>" title="<?= $nomeSite ?>">
            <p class="footer-p">é um parceiro</p>
            <img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
        </div>
        <div class="selos">
            <a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank"
                title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
            <a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>"
                target="_blank" title="CSS W3C"><i class="fab fa-css3"></i> <strong>W3C</strong></a>
        </div>
    </div>
</div>
<a id="scrollUp" href="#top" style="bottom:-80px;"><i class="fa fa-chevron-up fa-lg" aria-hidden="true"></i></a>
<script>
    <?php 
        include "js/geral.js";
        include "js/app.js";
        include "js/jquery.slicknav.js";
        include "js/modernizr-2.6.2.min.js";
        include "js/click-actions.js";
    ?>
</script>
<script defer src="<?= $url ?>js/jquery.slicknav.js"></script>
<script><? include("js/modernizr-2.6.2.min.js"); ?></script>
<script><? include("js/click-actions.js"); ?></script>

<script src="//code.jivosite.com/widget/5dJL0ENifV" async></script>

<!-- Shark Orcamento -->
<div id="sharkOrcamento" style="display: none;"></div>
<script>
    var guardar = document.querySelectorAll('.botao-cotar');
    for (var i = 0; i < guardar.length; i++) {
        guardar[i].removeAttribute('href');
        var adicionando = guardar[i].parentNode;
        adicionando.classList.add('nova-api');
    };
</script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-M0X26DWZKG"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'G-M0X26DWZKG');
</script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'G-D23WW3S4NC');
</script>



<!-- scrollup -->
<script>
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {
                $("#scrollUp").attr("style", "bottom:0;transition:0.5s;");
            }
            else {
                $("#scrollUp").attr("style", "bottom:-80px;transition:0.5s;");
            }
        });
        $("#scrollUp").click(function () {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });
    });
</script>


<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>

<!-- Script Launch start -->
<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script>
    const aside = document.querySelector('aside');
    const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
    aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
</script>
<!-- Script Launch end -->
<div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function () {
        $("html").mouseleave(function () {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function () {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>