<?
$nomeSite			= 'Portal das Gravações Industriais';
$slogan				= 'Simplesmente o melhor do ramo!';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-123172787-36';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


//Breadcrumbs
include('inc/categoriasGeral.php');
$caminho  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho .= '<a rel="home" itemprop="url" href="'.$url.'" title="Home">';
$caminho .= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div>';

$caminho2	 = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho2	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminho2	.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminho2	.= '<span itemprop="title"> Produtos </span></a> »';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div>';

$caminhodatador_de_hot_stamping = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhodatador_de_hot_stamping.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhodatador_de_hot_stamping.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhodatador_de_hot_stamping.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatador_de_hot_stamping.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhodatador_de_hot_stamping.= '<span itemprop="title"> Produtos </span></a> »';
$caminhodatador_de_hot_stamping.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatador_de_hot_stamping.= '<a href="'.$url.'datador-de-hot-stamping-categoria" title="Categoria - Datador de Hot Stamping" class="category" itemprop="url">';
$caminhodatador_de_hot_stamping.= '<span itemprop="title"> Categoria - Datador de Hot Stamping </span></a> »';
$caminhodatador_de_hot_stamping.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatador_de_hot_stamping.= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhodatadores  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhodatadores .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhodatadores .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhodatadores .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatadores .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhodatadores .= '<span itemprop="title"> Produtos </span></a> »';
$caminhodatadores .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatadores .= '<a href="'.$url.'datadores-categoria" title="Categoria - Datadores " class="category" itemprop="url">';
$caminhodatadores .= '<span itemprop="title"> Categoria - Datadores </span></a> »';
$caminhodatadores .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhodatadores .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhogravacoes_industriais  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhogravacoes_industriais .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhogravacoes_industriais .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhogravacoes_industriais .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogravacoes_industriais .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhogravacoes_industriais .= '<span itemprop="title"> Produtos </span></a> »';
$caminhogravacoes_industriais .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogravacoes_industriais .= '<a href="'.$url.'gravacoes-industriais-categoria" title="Categoria - Gravações Industriais " class="category" itemprop="url">';
$caminhogravacoes_industriais .= '<span itemprop="title"> Categoria - Gravações Industriais </span></a> »';
$caminhogravacoes_industriais .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogravacoes_industriais .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';

$caminhonumerador_industrial  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhonumerador_industrial .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhonumerador_industrial .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhonumerador_industrial .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhonumerador_industrial .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhonumerador_industrial .= '<span itemprop="title"> Produtos </span></a> »';
$caminhonumerador_industrial .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhonumerador_industrial .= '<a href="'.$url.'numerador-industrial-categoria" title="Categoria - Numerador Industrial " class="category" itemprop="url">';
$caminhonumerador_industrial .= '<span itemprop="title"> Categoria - Numerador Industrial </span></a> »';
$caminhonumerador_industrial .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhonumerador_industrial .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';
    
$caminhoroldanas_de_gravacoes  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhoroldanas_de_gravacoes .= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminhoroldanas_de_gravacoes .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminhoroldanas_de_gravacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoroldanas_de_gravacoes .= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhoroldanas_de_gravacoes .= '<span itemprop="title"> Produtos </span></a> »';
$caminhoroldanas_de_gravacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoroldanas_de_gravacoes .= '<a href="'.$url.'roldanas-de-gravacoes-categoria" title="Categoria - Roldanas de Gravações " class="category" itemprop="url">';
$caminhoroldanas_de_gravacoes .= '<span itemprop="title"> Categoria - Roldanas de Gravações </span></a> »';
$caminhoroldanas_de_gravacoes .= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhoroldanas_de_gravacoes .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div></div></div></div>';
    


?>